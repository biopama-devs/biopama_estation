(function ($, Drupal) {
    var eStationCards = drupalSettings.eStationCards;

    Drupal.behaviors.estationLayers = {
		attach: function (context, settings) {
            $(context).find(".indicator-content-wrapper").once("add-estation-behaviour").each(function () {
                var thisTitle = $(context).find(".indicator-card-title").text().trim();

                var iseStation = thisTitle.indexOf("eStation Data Browser");

                if (iseStation > -1){

                    _addMapLayer(thisMap, "biopamaWDPAPolyJRC");

                    var popupOptions = {
                        countryLink: '/ct/country/',
                        paLink: '',
                    }
                    $().addMapLayerInteraction(thisMap, popupOptions);

					var nodeID = $(this).find(".node_id").text().trim();
                    var cardWrapper = "#id-" + nodeID;

                    var titleNoSpace = thisTitle.replace(/\s/g, '');
                    var titleArray = titleNoSpace.split("-");
                    var selectedCategory = titleArray[1];
                    var selectedTopic = titleArray[2];
                    var topicID = "#collapse" + selectedTopic;
            
                    var eStationURL = "https://estation.jrc.ec.europa.eu/eStation3/analysis/gettimeseriesprofile";

                    var subproductsSelectList = document.createElement("select");
                    subproductsSelectList.id = "eStation-"+selectedTopic+"-subproducts";
                    $(cardWrapper + " .indicator-card-title").append( $( subproductsSelectList ) );
                    $( subproductsSelectList ).addClass("form-control");
                    var option = document.createElement("option");
                    option.value = null;
                    option.text = "Select a product";
                    subproductsSelectList.appendChild(option);
                    console.log(eStationCards);

                    for (var i = 0; i < eStationCards[selectedCategory].subproducts.length; i++) {
                        $("#eStation-"+selectedTopic+"-subproducts").append('<option value="'+eStationCards[selectedCategory].subproducts[i].code + '">' + eStationCards[selectedCategory].subproducts[i].name + '</option>'); 
                    }

                    $( "#eStation-"+selectedTopic+"-subproducts" ).change(function() {
                        if ($("#eStation-"+selectedTopic+"-years").length > 0){
                            $("#eStation-"+selectedTopic+"-years").remove();
                        }
            
                        var selectedSubProductName = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").text();
                        var selectedSubProductVal = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").val();
            
                        var prodList = eStationCards[selectedCategory].subproducts
                        var thisProd = prodList.filter(obj => {
                            return obj.name === selectedSubProductName
                        });
                        //console.log(thisProd)
            
                        if ($('#estation-'+selectedTopic+'-data-source').length > 0){
                            $('#estation-'+selectedTopic+'-data-source').remove();
                        }
            
                        var infoIcon = '<a href="'+ ( thisProd[0].hasOwnProperty('link') ? thisProd[0].link : eStationCards[selectedCategory].link ) +'" target="_blank" id="estation-'+selectedTopic+'-data-source" class="estation-data-source btn btn-sm btn-secondary p-0" style="--bs-btn-border-radius: 50%;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="right" aria-label="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'" data-bs-original-title="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'">'+
                                    '<i class="fa-solid fa-database"></i>'+
                                '</a>';
                        $(topicID).closest(".card-body").find(".info-icon").append( $( infoIcon ) );
            
                        var dataSource = document.getElementById('estation-'+selectedTopic+'-data-source')
                        //bootstrap.Tooltip.getOrCreateInstance(dataSource);
            
                        var description = thisProd[0].description;
                        $(topicID).find("#estation-description").empty().append( description );
                        
            
                        eStationMap(selectedTopic, selectedSubProductName);
                        
                        var yearsSelectList = document.createElement("select");
                        yearsSelectList.id = "eStation-"+selectedTopic+"-years";
                        $(cardWrapper + " .indicator-card-title").append( $( yearsSelectList ) );
                        $( yearsSelectList ).addClass("form-control w-25 estation-years");
             
                        for (var i = 0; i < 20; i++) {
                            var thisYear = moment().startOf('year').subtract(i, 'years').format('YYYY')
                            $("#eStation-"+selectedTopic+"-years").append('<option value="' + thisYear + '">' + thisYear + '</option>'); 
                        }
            
            
                        $("#eStation-"+selectedTopic+"-years").change(function() {
                            $().insertBiopamaLoader("#estation-"+selectedTopic+"-chart"); 
            
                            var selectedYear = $( "#eStation-"+selectedTopic+"-years" ).find(":selected").val();
                            var xhr = new XMLHttpRequest();
                            xhr.open("POST", eStationURL);
            
                            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            
                            xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4) {
                                //console.log(xhr.status);
                                //console.log(xhr.responseText);
                                eStationChart(selectedTopic, selectedSubProductName, xhr.responseText)
                            }};

                            $.when(
                                $.getJSON(WKTurl,function(d){
                                    if (!$.isEmptyObject(d)){
                                        thisWKT = d['features'][0]['properties']['wkt_geometry']
                                        console.log(thisWKT);
                                    }
                                }).fail(function (result) {
                                    console.log('unable to get WKT'); // this is hit for return Json(null); and return null;
                                }),
                            ).then(function() {
                                var data = eStationData(selectedSubProductVal, selectedYear);
                                xhr.send(data);
                            });
                            
                        });
                        $("#eStation-"+selectedTopic+"-years").trigger('change');
                    });

                    function eStationData(product, year){

                        var prodList = eStationCards[selectedCategory].subproducts
                        var thisProd = prodList.filter(obj => {
                            return obj.code === product
                        })
            
                        var data = "productcode=" + ( thisProd[0].hasOwnProperty('productcode') ? thisProd[0].productcode : eStationCards[selectedCategory].productcode ) +
                        "&version=" + ( thisProd[0].hasOwnProperty('version') ? thisProd[0].version : eStationCards[selectedCategory].version ) +
                        "&subproductcode="+product+
                        "&mapsetcode=" + ( thisProd[0].hasOwnProperty('mapsetcode') ? thisProd[0].mapsetcode : eStationCards[selectedCategory].mapsetcode ) +
                        "&yearsToCompare=%5B"+year+"%5D"+
                        "&WKT="+thisWKT;
                        console.log(data)
                        return data;
                    }

                    function eStationMap(topic, product){
                        var prodList = eStationCards[selectedCategory].subproducts
                        
                        var thisProd = prodList.filter(obj => {
                            return obj.name === product
                        })
            
                        if (thisMap.getLayer("eStation-layer-b10p4m4" + nodeID)) {
                            thisMap.removeLayer("eStation-layer-b10p4m4" + nodeID);
                        }
                        if (thisMap.getSource("eStation-source")) {
                            thisMap.removeSource("eStation-source");
                        }
            
                        if (thisProd[0].mapLayer !== ''){
                            thisMap.addSource('eStation-source', {
                                'type': 'raster',
                                'tiles': [
                                'https://estation.jrc.ec.europa.eu/eStation3/webservices?SERVICE=WMS&&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png'+
                                '&LAYERS=' + thisProd[0].mapLayer +
                                '&TRANSPARENT=true&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}'
                                ],
                                'tileSize': 256,
                            });
                            thisMap.addLayer(
                                {
                                "id": "eStation-layer-b10p4m4" + nodeID,
                                'type': 'raster',
                                'source': 'eStation-source',
                                'paint': {}
                                }, 'wdpaAcpSelected'
                            );
            
                            var legendURL = 'https://estation.jrc.ec.europa.eu/eStation3/mobile-app/legend-html?product_id=' + thisProd[0].mapLayer
                            $.ajax({
                                url: legendURL,
                                dataType: 'json',
                                success: function(d) {
                                    //console.log(d.html.legendHTML)
                                    $("#estation-"+selectedTopic+"-legend").empty().append( d.html.legendHTML );
                                    $("#estation-"+selectedTopic+"-legend table").addClass("w-100");
                                },
                                error: function() {
                                  console.log("Something is wrong with the REST servce for the map Legend")
                                }
                            });
                        } else {
                            var message = '<div class="alert alert-info" role="alert">'+
                                'no map layer'+
                            '</div>';
                            $("#estation-"+selectedTopic+"-legend").empty().append( message );
                        }
                    }

                    function eStationChart(topic, product, response){
                        var selectedTopic = topic.replace(/\s/g, '');
                        $().removeBiopamaLoader("#estation-"+selectedTopic+"-chart"); 
                        var eStationData = JSON.parse(response);
                        var timeSeries = eStationData.timeseriesvalue;
                        var dates = [];
                        var values = [];
                        timeSeries.forEach(function (arrayItem) {
                            dates.push(moment(arrayItem.date, 'YYYYMMDD').format('DD/MM/YYYY'));
                            var val = arrayItem.value
                            if (val !== null){
                                valRound = val.toFixed(2);
                                values.push(valRound);
                            } else {
                                values.push(val);
                            }
                        });
                        var prodList = eStationCards[selectedCategory].subproducts
                        var thisProd = prodList.filter(obj => {
                            return obj.name === product
                        })
                        
                        var chartData = {
                            title: product + " for " +  selSettings.paName,
                            yAxis: [
                                {
                                    position: "right",
                                    alignTicks: true,
                                    nameLocation: 'middle',
                                    nameGap: 70,
                                    axisLabel: {
                                        formatter: '{value}',
                                        color: biopamaGlobal.chart.colors.text
                                    }
                                }
                            ],
                            xAxis: [
                                {
                                    type: 'category',
                                    data: dates
                                }
                            ],
                            series: [
                                {
                                    name: 'value',
                                    type: thisProd[0].chartType,
                                    data: values
                                },
                            ]
                        }
                        //console.log(selectedTopic)
                        $().createXYAxisChart("estation-"+selectedTopic+"-chart" , chartData);  
                        
                    }
            

                    // var selectSpecies = '<select id="gbif-species" class="form-control w-100 mt-2"><option value="0"> Select an Animal Phylum</option><option value="1"> All Animals</option><option value="42">Annelids (Annelida) </option><option value="54">Arthropods (Arthropoda)</option><option value="110">Bivalve coelomates (Brachiopoda)</option><option value="53">Bryozoan (Bryozoa)</option><option value="7423964">Cephalorhyncha (Cephalorhyncha)</option><option value="55">Arrow worm (Chaetognatha)</option><option value="44">Chordates (Chordata)</option><option value="43">Cnidarians (Cnidaria)</option><option value="51">Comb jellies (Ctenophora)</option><option value="45">Cycliophorans (Cycliophora)</option><option value="7663989">Dicyemida (Dicyemida)</option><option value="50">Echinodermata (Echinoderms)</option><option value="8173593">Entoprocts (Entoprocta)</option><option value="22">Gastrotricha (Gastrotricha)</option><option value="77">Gnathostomulids (Gnathostomulida)</option><option value="75">Acorn worm (Hemichordata)</option><option value="52">Molluscs (Mollusca)</option><option value="66">Myxozoa (Myxozoa)</option><option value="5967481">Roundworm (Nematoda)</option><option value="64">Gordian worms (Nematomorpha)</option><option value="63">Proboscis worms (Nemertea)</option><option value="62">Onychophorans (Onychophora)</option><option value="212">Horseshoe worms (Phoronida)</option><option value="105">Sponge (Porifera)</option><option value="91">Rotifers (Rotifera)</option><option value="74">Peanut worms (Sipuncula)</option><option value="14">Tardigrades (Tardigrada)</option><option value="7190138">Xenacoelomorpha (Xenacoelomorpha) </option></select>';
                    // $(cardWrapper + " .indicator-card-title").append( $( selectSpecies ) );
                    // var selectPlants = '<select id="gbif-plants" class="form-control w-100 mt-2"><option value="0"> Select a Plant Phylum</option><option value="6"> All Plants</option><option value="13">Hornworts (Anthocerotophyta) </option><option value="35">Hornworts (Bryophyta)</option><option value="7819616">Brittleworts (Charophyta)</option><option value="53">Bryozoan (Chlorophyta)</option><option value="36">Green algae (Cephalorhyncha)</option><option value="37">Glaucophytes (Glaucophyta)</option><option value="9">Liverworts (Marchantiophyta)</option><option value="106">Red algae (Rhodophyta)</option><option value="7707728">Tracheophytes (Tracheophyta)</option></select>';
                    // $(cardWrapper + " .indicator-card-title").append( $( selectPlants ) );
                    // var yearStart = '<input type="number" id="yearStart" class="form-control" name="Start" min="1900" max="2024" placeholder="1900" value="1900">';
                    // var yearEnd = '<input type="number" id="yearEnd" class="form-control" name="End" min="1900" max="2024" placeholder="2024" value="2024">';
                    // var yearSelect = '<div class="row mt-2"><div class="col text-end lh-lg">Start</div><div class="col">'+yearStart+'</div><div class="col text-end lh-lg">End</div><div class="col">'+yearEnd+'</div></div>'
                    // $(cardWrapper + " .indicator-card-title").append( $( yearSelect ) );

                }
            });	
	    }
    };


    
    $('.estation-data-card').on('shown.bs.collapse', function () {

        var selectedTopic = $(this).closest( ".card-body" ).find(".inner-card-title").text().trim();
        var selectedTopic = selectedTopic.replace(/\s/g, '');
        var selectedCategory = $(this).closest( ".wrapper-card" ).prev().text().trim();
        var topicID = "#collapse" + selectedTopic;
        var topicID = topicID.replace(/\s/g, '');

        if ($("#eStation-"+selectedTopic+"-subproducts").length > 0){
            var selectedSubProductName = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").text();
            eStationMap(selectedTopic, selectedSubProductName);
            return;
        }

        var eStationURL = "https://estation.jrc.ec.europa.eu/eStation3/analysis/gettimeseriesprofile";

        if ($(topicID +" #eStation-subproducts").length > 0){
            $(topicID +" #eStation-subproducts").remove();
        }
        if ($(topicID +" #eStation-years").length > 0){
            $(topicID +" #eStation-years").remove();
        }
        if ($(topicID +" .estation-data-source").length > 0){
            $(topicID +" .estation-data-source").remove();
        }
        var subproductsSelectList = document.createElement("select");
        subproductsSelectList.id = "eStation-"+selectedTopic+"-subproducts";
        $(this).find(".estation-results").append( $( subproductsSelectList ) );
        $( subproductsSelectList ).addClass("form-control");
        var option = document.createElement("option");
        option.value = null;
        option.text = "Select a product";
        subproductsSelectList.appendChild(option);


        for (var i = 0; i < eStationCards[selectedCategory].subproducts.length; i++) {
            $("#eStation-"+selectedTopic+"-subproducts").append('<option value="'+eStationCards[selectedCategory].subproducts[i].code + '">' + eStationCards[selectedCategory].subproducts[i].name + '</option>'); 
        }

        $( "#eStation-"+selectedTopic+"-subproducts" ).change(function() {
            if ($("#eStation-"+selectedTopic+"-years").length > 0){
                $("#eStation-"+selectedTopic+"-years").remove();
            }

            var selectedSubProductName = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").text();
            var selectedSubProductVal = $( "#eStation-"+selectedTopic+"-subproducts" ).find(":selected").val();

            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.name === selectedSubProductName
            });
            //console.log(thisProd)

            if ($('#estation-'+selectedTopic+'-data-source').length > 0){
                $('#estation-'+selectedTopic+'-data-source').remove();
            }

            var infoIcon = '<a href="'+ ( thisProd[0].hasOwnProperty('link') ? thisProd[0].link : eStationCards[selectedCategory].link ) +'" target="_blank" id="estation-'+selectedTopic+'-data-source" class="estation-data-source btn btn-sm btn-secondary p-0" style="--bs-btn-border-radius: 50%;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="right" aria-label="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'" data-bs-original-title="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'">'+
                        '<i class="fa-solid fa-database"></i>'+
                    '</a>';
            $(topicID).closest(".card-body").find(".info-icon").append( $( infoIcon ) );

            var dataSource = document.getElementById('estation-'+selectedTopic+'-data-source')
            bootstrap.Tooltip.getOrCreateInstance(dataSource);

            var description = thisProd[0].description;
            $(topicID).find("#estation-description").empty().append( description );
            

            eStationMap(selectedTopic, selectedSubProductName);
            
            var yearsSelectList = document.createElement("select");
            yearsSelectList.id = "eStation-"+selectedTopic+"-years";
            $(topicID).find(".estation-results").append( $( yearsSelectList ) );
            $( yearsSelectList ).addClass("form-control w-25 estation-years");
            //var selectedTopic = $( "#eStation-Topics" ).find(":selected").val();
            //var years = eStationCards[selectedTopic].years;
            //years.reverse(); // so the most recent year appears first
            
            for (var i = 0; i < 20; i++) {
                var thisYear = moment().startOf('year').subtract(i, 'years').format('YYYY')
                $("#eStation-"+selectedTopic+"-years").append('<option value="' + thisYear + '">' + thisYear + '</option>'); 
            }


            $("#eStation-"+selectedTopic+"-years").change(function() {
                $().insertBiopamaLoader("#estation-"+selectedTopic+"-chart"); 

                var selectedYear = $( "#eStation-"+selectedTopic+"-years" ).find(":selected").val();
                var xhr = new XMLHttpRequest();
                xhr.open("POST", eStationURL);

                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

                xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    //console.log(xhr.status);
                    //console.log(xhr.responseText);
                    eStationChart(selectedTopic, selectedSubProductName, xhr.responseText)
                }};

                var data = eStationData(selectedSubProductVal, selectedYear);
                xhr.send(data);
                
            });
            $("#eStation-"+selectedTopic+"-years").trigger('change');
        });

        function eStationData(product, year){

            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.code === product
            })

            var data = "productcode=" + ( thisProd[0].hasOwnProperty('productcode') ? thisProd[0].productcode : eStationCards[selectedCategory].productcode ) +
            "&version=" + ( thisProd[0].hasOwnProperty('version') ? thisProd[0].version : eStationCards[selectedCategory].version ) +
            "&subproductcode="+product+
            "&mapsetcode=" + ( thisProd[0].hasOwnProperty('mapsetcode') ? thisProd[0].mapsetcode : eStationCards[selectedCategory].mapsetcode ) +
            "&yearsToCompare=%5B"+year+"%5D"+
            "&WKT="+thisWKT;
            console.log(data)
            return data;
        }




    });
    $('.estation-data-card').on('hide.bs.collapse', function () {
        var selectedTopic = $(this).closest( ".card-body" ).find(".inner-card-title").text().trim();
        var selectedTopic = selectedTopic.replace(/\s/g, '');
        if (thisMap.getLayer("eStation-layer")) {
            thisMap.removeLayer("eStation-layer");
        }
        if (thisMap.getSource("eStation-source")) {
            thisMap.removeSource("eStation-source");
        }
        if ($('#estation-'+selectedTopic+'-data-source').length > 0){
            $('#estation-'+selectedTopic+'-data-source').remove();
        }
    });

})(jQuery, Drupal);