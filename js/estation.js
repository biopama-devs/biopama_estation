(function ($, Drupal) {
    var eStationCards = drupalSettings.eStationCards;

    $('.estation-data-card').on('shown.bs.collapse', function () {
        var selectedTopic = $(this).closest( ".card-body" ).find(".inner-card-title").text().trim();
        var selectedTopicNoSpace = selectedTopic.replace(/\s/g, '');
        var selectedCategory = $(this).closest( ".wrapper-card" ).prev().text().trim();
        var topicID = "#collapse" + selectedTopic;
        var topicIDNoSpace = topicID.replace(/\s/g, '');

        if ($("#eStation-"+selectedTopicNoSpace+"-subproducts").length > 0){
            var selectedSubProductName = $( "#eStation-"+selectedTopicNoSpace+"-subproducts" ).find(":selected").text();
            eStationMap(selectedTopic, selectedSubProductName);
            return;
        }

        var eStationURL = "https://estation.jrc.ec.europa.eu/eStation3/analysis/gettimeseriesprofile";

        if ($(topicIDNoSpace +" #eStation-subproducts").length > 0){
            $(topicIDNoSpace +" #eStation-subproducts").remove();
        }
        if ($(topicIDNoSpace +" #eStation-years").length > 0){
            $(topicIDNoSpace +" #eStation-years").remove();
        }
        if ($(topicIDNoSpace +" .estation-data-source").length > 0){
            $(topicIDNoSpace +" .estation-data-source").remove();
        }
        var subproductsSelectList = document.createElement("select");
        subproductsSelectList.id = "eStation-"+selectedTopicNoSpace+"-subproducts";
        $(this).find(".estation-results").append( $( subproductsSelectList ) );
        $( subproductsSelectList ).addClass("form-control");
        var option = document.createElement("option");
        option.value = null;
        option.text = "Select a product";
        subproductsSelectList.appendChild(option);


        for (var i = 0; i < eStationCards[selectedCategory].subproducts.length; i++) {
            $("#eStation-"+selectedTopicNoSpace+"-subproducts").append('<option value="'+eStationCards[selectedCategory].subproducts[i].code + '">' + eStationCards[selectedCategory].subproducts[i].name + '</option>'); 
        }

        $( "#eStation-"+selectedTopicNoSpace+"-subproducts" ).change(function() {
            if ($("#eStation-"+selectedTopicNoSpace+"-years").length > 0){
                $("#eStation-"+selectedTopicNoSpace+"-years").remove();
            }

            var selectedSubProductName = $( "#eStation-"+selectedTopicNoSpace+"-subproducts" ).find(":selected").text();
            var selectedSubProductVal = $( "#eStation-"+selectedTopicNoSpace+"-subproducts" ).find(":selected").val();

            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.name === selectedSubProductName
            });
            //console.log(thisProd)

            if ($('#estation-'+selectedTopicNoSpace+'-data-source').length > 0){
                $('#estation-'+selectedTopicNoSpace+'-data-source').remove();
            }

            var infoIcon = '<a href="'+ ( thisProd[0].hasOwnProperty('link') ? thisProd[0].link : eStationCards[selectedCategory].link ) +'" target="_blank" id="estation-'+selectedTopicNoSpace+'-data-source" class="estation-data-source btn btn-sm btn-secondary p-0" style="--bs-btn-border-radius: 50%;" data-bs-toggle="tooltip" data-bs-html="true" data-bs-placement="right" aria-label="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'" data-bs-original-title="Data Source: ' + ( thisProd[0].hasOwnProperty('provider') ? thisProd[0].provider : eStationCards[selectedCategory].provider ) +'">'+
                        '<i class="fa-solid fa-database"></i>'+
                    '</a>';
            $(topicIDNoSpace).closest(".card-body").find(".info-icon").append( $( infoIcon ) );

            var dataSource = document.getElementById('estation-'+selectedTopicNoSpace+'-data-source')
            bootstrap.Tooltip.getOrCreateInstance(dataSource);

            var description = thisProd[0].description;
            $(topicIDNoSpace).find("#estation-description").empty().append( description );
            

            eStationMap(selectedTopic, selectedSubProductName);
            
            var yearsSelectList = document.createElement("select");
            yearsSelectList.id = "eStation-"+selectedTopicNoSpace+"-years";
            $(topicIDNoSpace).find(".estation-results").append( $( yearsSelectList ) );
            $( yearsSelectList ).addClass("form-control w-25 estation-years");
            //var selectedTopic = $( "#eStation-Topics" ).find(":selected").val();
            //var years = eStationCards[selectedTopic].years;
            //years.reverse(); // so the most recent year appears first
            
            for (var i = 0; i < 20; i++) {
                var thisYear = moment().startOf('year').subtract(i, 'years').format('YYYY')
                $("#eStation-"+selectedTopicNoSpace+"-years").append('<option value="' + thisYear + '">' + thisYear + '</option>'); 
            }


            $("#eStation-"+selectedTopicNoSpace+"-years").change(function() {
                $().insertBiopamaLoader("#estation-"+selectedTopicNoSpace+"-chart"); 

                var selectedYear = $( "#eStation-"+selectedTopicNoSpace+"-years" ).find(":selected").val();
                var xhr = new XMLHttpRequest();
                xhr.open("POST", eStationURL);

                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

                xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    //console.log(xhr.status);
                    //console.log(xhr.responseText);
                    eStationChart(selectedTopic, selectedSubProductName, xhr.responseText)
                }};

                var data = eStationData(selectedSubProductVal, selectedYear);
                xhr.send(data);
                
            });
            $("#eStation-"+selectedTopicNoSpace+"-years").trigger('change');
        });

        function eStationData(product, year){

            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.code === product
            })

            var data = "productcode=" + ( thisProd[0].hasOwnProperty('productcode') ? thisProd[0].productcode : eStationCards[selectedCategory].productcode ) +
            "&version=" + ( thisProd[0].hasOwnProperty('version') ? thisProd[0].version : eStationCards[selectedCategory].version ) +
            "&subproductcode="+product+
            "&mapsetcode=" + ( thisProd[0].hasOwnProperty('mapsetcode') ? thisProd[0].mapsetcode : eStationCards[selectedCategory].mapsetcode ) +
            "&yearsToCompare=%5B"+year+"%5D"+
            "&WKT="+thisWKT;
            console.log(data)
            return data;
        }

        function eStationChart(topic, product, response){
            var selectedTopicNoSpace = topic.replace(/\s/g, '');
            $().removeBiopamaLoader("#estation-"+selectedTopicNoSpace+"-chart"); 
            var eStationData = JSON.parse(response);
            var timeSeries = eStationData.timeseriesvalue;
            var dates = [];
            var values = [];
            timeSeries.forEach(function (arrayItem) {
                dates.push(moment(arrayItem.date, 'YYYYMMDD').format('DD/MM/YYYY'));
                var val = arrayItem.value
                if (val !== null){
                    valRound = val.toFixed(2);
                    values.push(valRound);
                } else {
                    values.push(val);
                }
            });
            var prodList = eStationCards[selectedCategory].subproducts
            var thisProd = prodList.filter(obj => {
                return obj.name === product
            })
            
            var chartData = {
                title: product + " for " +  selSettings.paName,
                yAxis: [
                    {
                        position: "right",
                        alignTicks: true,
                        nameLocation: 'middle',
                        nameGap: 70,
                        axisLabel: {
                            formatter: '{value}',
                            color: biopamaGlobal.chart.colors.text
                        }
                    }
                ],
                xAxis: [
                    {
                        type: 'category',
                        data: dates
                    }
                ],
                series: [
                    {
                        name: 'value',
                        type: thisProd[0].chartType,
                        data: values
                    },
                ]
            }
            //console.log(selectedTopicNoSpace)
            $().createXYAxisChart("estation-"+selectedTopicNoSpace+"-chart" , chartData);  
            
        }

        function eStationMap(topic, product){
            var prodList = eStationCards[selectedCategory].subproducts
            
            var thisProd = prodList.filter(obj => {
                return obj.name === product
            })

            if (mymap.getLayer("eStation-layer")) {
                mymap.removeLayer("eStation-layer");
            }
            if (mymap.getSource("eStation-source")) {
                mymap.removeSource("eStation-source");
            }

            if (thisProd[0].mapLayer !== ''){
                mymap.addSource('eStation-source', {
                    'type': 'raster',
                    'tiles': [
                    'https://estation.jrc.ec.europa.eu/eStation3/webservices?SERVICE=WMS&&VERSION=1.3.0&REQUEST=GetMap&FORMAT=image/png'+
                    '&LAYERS=' + thisProd[0].mapLayer +
                    '&TRANSPARENT=true&WIDTH=256&HEIGHT=256&CRS=EPSG:3857&BBOX={bbox-epsg-3857}'
                    ],
                    'tileSize': 256,
                });
                mymap.addLayer(
                    {
                    'id': 'eStation-layer',
                    'type': 'raster',
                    'source': 'eStation-source',
                    'paint': {}
                    }, 'wdpaAcpSelected'
                );

                var legendURL = 'https://estation.jrc.ec.europa.eu/eStation3/mobile-app/legend-html?product_id=' + thisProd[0].mapLayer
                $.ajax({
                    url: legendURL,
                    dataType: 'json',
                    success: function(d) {
                        //console.log(d.html.legendHTML)
                        $("#estation-"+selectedTopicNoSpace+"-legend").empty().append( d.html.legendHTML );
                        $("#estation-"+selectedTopicNoSpace+"-legend table").addClass("w-100");
                    },
                    error: function() {
                      console.log("Something is wrong with the REST servce for the map Legend")
                    }
                });
            } else {
                var message = '<div class="alert alert-info" role="alert">'+
                    'no map layer'+
                '</div>';
                $("#estation-"+selectedTopicNoSpace+"-legend").empty().append( message );
            }
        }
    });
    $('.estation-data-card').on('hide.bs.collapse', function () {
        var selectedTopic = $(this).closest( ".card-body" ).find(".inner-card-title").text().trim();
        var selectedTopicNoSpace = selectedTopic.replace(/\s/g, '');
        if (mymap.getLayer("eStation-layer")) {
            mymap.removeLayer("eStation-layer");
        }
        if (mymap.getSource("eStation-source")) {
            mymap.removeSource("eStation-source");
        }
        if ($('#estation-'+selectedTopicNoSpace+'-data-source').length > 0){
            $('#estation-'+selectedTopicNoSpace+'-data-source').remove();
        }
    });

})(jQuery, Drupal);